function start() {
    const submit = document.getElementById("submit");
    submit.addEventListener("click", () => {
        Receipt.addProduct();
    });
    
    Receipt.getProducts();
}

class Receipt {
    static products = new Array();
    
    static getInputs() {
        const inputName = document.getElementById("pname");
        const inputPrice = document.getElementById("price");
        const inputAmount = document.getElementById("amount");
        
        const inputs = {
            name: inputName,
            price: inputPrice,
            amount: inputAmount
        }
        
        return inputs;
    }

    static addProduct() {
        const inputs = Receipt.getInputs();
        let submit = document.getElementById("submit");
        
        let product = new Product(inputs.name.value, inputs.price.value, inputs.amount.value);
        
        Receipt.products.push(product);
        Receipt.refreshReceipt();
        Receipt.sumUpdate();
    }

    static getProducts() {
        Receipt.products = JSON.parse(localStorage.getItem("Products") || "[]");
        Receipt.refreshReceipt();
    }
    
    static deleteProduct(id) {
        Receipt.products.splice(id,1);
        Receipt.refreshReceipt();    
    }

    static changeInputsForUpdate(id) {
        const inputs = Receipt.getInputs();
  
        inputs.name.value = Receipt.products[id].name;
        inputs.price.value = Receipt.products[id].price;
        inputs.amount.value = Receipt.products[id].amount;
         
        Receipt.changeToUpdate(id);
    }
        
    static updateProduct(id) {
        const inputs = Receipt.getInputs();

        Receipt.products[id].name = inputs.name.value;
        Receipt.products[id].amount = inputs.price.value;
        Receipt.products[id].price = inputs.amount.value;
        
        Receipt.changeToAdd();
        Receipt.refreshReceipt();
    }

    static changeToUpdate(id) {
        let submit = document.getElementById("submit");
        let newSubmit = submit.cloneNode(true);
        submit.parentNode.replaceChild(newSubmit, submit);
        newSubmit.addEventListener("click", () => {
            Receipt.updateProduct(id);
        });
    }

    static changeToAdd() {
        let submit = document.getElementById("submit");
        let newSubmit = submit.cloneNode(true);
        submit.parentNode.replaceChild(newSubmit, submit);
        newSubmit.addEventListener("click", () => {
            Receipt.addProduct();
        });
    }

    static sumUpdate() {
        let sumDiv = document.getElementById("sum");
        let sum = 0;
        Receipt.products.forEach((element) => {
            sum += element.price * element.amount;
        });
        
        sumDiv.innerHTML = sum.toFixed(2);
    }

    static refreshReceipt() { 
        let divReceipt = document.getElementById("products");
        divReceipt.innerHTML = "";
        Receipt.products.forEach((product, id) => {
            product.id = id;
            divReceipt.innerHTML += Product.getHtml(product);
        });
        
        Receipt.sumUpdate();
        Receipt.clearProducts();
        Receipt.saveProducts();
    }

    static clearProducts() {
        const inputs = Receipt.getInputs();
        
        inputs.name.value = "";
        inputs.price.value = "";
        inputs.amount.value = "";
    }

    static saveProducts() {
        localStorage.setItem("Products", JSON.stringify(Receipt.products));
    }
}

class Product {

    constructor(name, price, amount) {
        this.name = name;
        this.price = price;
        this.amount = amount;
        this.id = 0;
    }
    
    static getHtml(product) {
        return `<div>
            <span>${product.name}</span>
            <span>${product.price}</span>
            <span>${product.amount}</span>
            <i onclick="Receipt.deleteProduct(${product.id})" class="icon-trash"></i>
            <i onclick="Receipt.changeInputsForUpdate(${product.id})" class="icon-pencil"></i>
        </div>`;
        
    }

}
start();